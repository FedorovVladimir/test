package main

import "testing"

func TestSum1And1(t *testing.T) {
	s := sum(1, 1)
	if s != 2 {
		t.Errorf("sum(1, 1) = %d; want 2", s)
	}
}

func TestSum1And2(t *testing.T) {
	s := sum(1, 2)
	if s != 3 {
		t.Errorf("sum(1, 2) = %d; want 3", s)
	}
}
